"""main.py."""

import h5py as h5
import numpy as np
import astropy.units as u


class RadameshPy:
    """Handling actions aiming at reading and processing RADAMESH outputs."""

    def __init__(self, path):
        """
        Initializing an instance of RadameshPy by loading attributes.

        parameters:
        path -- path to a RADAMESH output
        """
        self.file = h5.File ( path, 'r' )

        self.attrs = self.__load_attrs ()

        self.num_levels = int ( self.attrs['num_levels'] )
        self.num_components = int ( self.attrs['num_components'] )
        self.ndim = int ( self.attrs['SpaceDim'] )
        self.z = float ( self.attrs['redshift'] )
        self.comoving_box_size = float ( self.attrs['ComovingBoxSize'] ) * u.Mpc

        self.comps = [ self.attrs['component_%d' % i] for i in range (self.num_components)]

        self.levels = [ dict() for _ in range(self.num_levels) ]

        for l in range ( self.num_levels ):
            level = self.levels[l]
            level_tag = "level_%d" % l

            chombo_attrs = self.file[level_tag].attrs
            level['attrs'] = {}

            level['attrs']['dx'] = chombo_attrs.get('dx')
            level['attrs']['ref_ratio'] = chombo_attrs.get('ref_ratio')

            if l == 0:
                level['attrs']['prob_domain'] = chombo_attrs.get('prob_domain')

            level['boxes_coor'] = self.file[level_tag]['boxes']
            level['boxes'] = [ dict() for _ in range ( len(level['boxes_coor']) ) ]
            level['data'] = []


    def __del__ ( self ):
        if isinstance ( self.file, h5.File ):
            self.file.close ()


    def close ( self ):
        self.file.close ()


    def load ( self ):
        """Loading all levels."""
        for l in range ( self.num_levels ):
            self.load_level (l)


    def load_level ( self, l ):
        """
        Loading different fields of one of the given levels of the chombo file.

        parameters:
        l -- level (integer)
        """

        self.levels[l]['data'] = self.file['level_%d' % l]['data:datatype=0'][:]


    def fill_boxes ( self ):
        for l in range ( self.num_levels ):
            self.__fill_level_boxes (l)


    def __fill_level_boxes ( self, l ):
        """
        Filling boxes from memory
        """

        if len (self.levels[l]['data']) == 0:
            self.load_level (l)

        level = self.levels[l]

        offset = 0
        for box, coord in zip ( level['boxes'], level['boxes_coor'] ):
            dims = [ x - y + 1 for x,y in zip (
                list(coord)[self.ndim:], list(coord)[:self.ndim]
            )]
            n_cells = np.prod ( dims )

            for i, comp in enumerate(self.comps):
                begin = offset + i * n_cells
                end = begin + n_cells

                box[comp] = np.array ( level['data'][begin:end] ).reshape(dims)

            offset += self.num_components * n_cells


    def __load_attrs(self):
        """Loading RADAMESH main attributes."""
        attrs = {}
        chombo_attrs = self.file['/'].attrs

        for key in chombo_attrs.keys():
            v = chombo_attrs.get(key)
            attrs[key] = v[0] if isinstance(v, list) and len(v) == 1 else v

        for i in range(attrs['num_components']):
            comp = 'component_%d' % i
            attrs[comp] = attrs[comp].decode("utf-8")

        attrs['SpaceDim'] = self.file['/Chombo_global'].attrs.get('SpaceDim')

        return attrs


    def lineout(self, x0, x1, field):
        """
        Constructing data points on a given line segment.

        parameters:
        x0 -- initial point of the line (tuple)
        x1 -- final point of the line (tuple)
        field -- field of interest
        """
        pass
