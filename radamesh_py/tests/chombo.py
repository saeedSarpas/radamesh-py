#!/usr/bin/env python
"""Generating a test RADAMESH (chombo) file for testing purposes."""

# to run this file go to the root directory of this package and run:
# $ python setup.py chombo

# +-------+-------+-------+-------+
# |_|_|   |       |       |       |
# |_|_|___|       |       |       |
# |   |   |       |       |       |
# |   |   |       |       |       |
# +-------+-------+-------+-------+
# |       |       |   |   |       |
# |       |       |___|___|       |
# |       |       |_|_|   |       |
# |       |       | | |   |       |
# +-------+-------+-------+-------+
# |       |   |_|_|_|_|   |       |
# |       |___|_|_|_|_|___|       |
# |       |   |   |   |   |       |
# |       |   |   |   |   |       |
# +-------+-------+-------+-------+
# |       |       |       |       |
# |       |       |       |       |
# |       |       |       |       |
# |       |       |       |       |
# +-------+-------+-------+-------+

import h5py
import numpy as np

# box type
boxtype = [
    ('lo_i', np.int),
    ('lo_j', np.int),
    ('hi_i', np.int),
    ('hi_j', np.int),
]

# Constants
FILENAME = 'radamesh_py/tests/assets/chombo.h5'
NUM_LEVELS = 3
INIT_DX = 0.25
REDSHIFT = 1.23
BOXSIZE = 25.0
INIT_PROB_DOMAIN = np.array([(0, 0, 3, 3)], dtype=boxtype)

BOXES = [
    [
        (0, 0, 3, 3)
    ],
    [
        (0, 6, 1, 7),
        (4, 4, 5, 5),
        (2, 2, 5, 3),
    ],
    [
        (0, 14, 1, 15),
        (8, 8, 9, 9),
        (6, 6, 9, 7),
    ]
]

DATA = [
    np.array ( [
        0.01 if i < 16 else 0.02 for i in range(32)
    ] ),
    np.array ( [
        1.01, 1.01, 1.01, 1.01, 1.02, 1.02, 1.02, 1.02,
        1.11, 1.11, 1.11, 1.11, 1.12, 1.12, 1.12, 1.12,
        1.21, 1.21, 1.21, 1.21, 1.21, 1.21, 1.21, 1.21, 1.22, 1.22, 1.22, 1.22, 1.22, 1.22, 1.22, 1.22
    ] ),
    np.array ( [
        2.01, 2.01, 2.01, 2.01, 2.02, 2.02, 2.02, 2.02,
        2.11, 2.11, 2.11, 2.11, 2.12, 2.12, 2.12, 2.12,
        2.21, 2.21, 2.21, 2.21, 2.21, 2.21, 2.21, 2.21, 2.22, 2.22, 2.22, 2.22, 2.22, 2.22, 2.22, 2.22
    ] ),
]


def create():
    """Creating a chombo file based on above constants."""
    chombo = h5py.File(FILENAME, 'w')

    # Chombo format attributes
    chombo.attrs['iteration'] = 1
    chombo.attrs['time'] = 1.23e4
    chombo.attrs['num_levels'] = NUM_LEVELS
    chombo.attrs['num_components'] = 2
    chombo.attrs['component_0'] = np.string_('field_0')
    chombo.attrs['component_1'] = np.string_('field_1')

    # Non-standard attributes
    chombo.attrs['redshift'] = REDSHIFT
    chombo.attrs['ComovingBoxSize'] = BOXSIZE

    chombo_global = chombo.create_group('Chombo_global')
    chombo_global.attrs['SpaceDim'] = 2

    # Extra attributes
    chombo.attrs['ProblemDomain'] = np.array([4, 4], dtype=np.int)

    # level 0
    lev_0 = chombo.create_group('level_0')

    lev_0.attrs['dx'] = INIT_DX
    lev_0.attrs['prob_domain'] = INIT_PROB_DOMAIN
    lev_0.attrs['ref_ratio'] = 2

    boxes_0 = lev_0.create_dataset('boxes', (1,), dtype=boxtype)

    boxes_0[0] = BOXES[0][0]

    lev_0['data:datatype=0'] = DATA[0]

    # level 1
    lev_1 = chombo.create_group('level_1')

    lev_1.attrs['dx'] = INIT_DX / 2
    lev_1.attrs['ref_ratio'] = 2

    boxes_1 = lev_1.create_dataset('boxes', (3,), dtype=boxtype)

    boxes_1[0] = BOXES[1][0]
    boxes_1[1] = BOXES[1][1]
    boxes_1[2] = BOXES[1][2]

    lev_1['data:datatype=0'] = DATA[1]

    # level 2
    lev_2 = chombo.create_group('level_2')

    lev_2.attrs['dx'] = INIT_DX / 4
    lev_2.attrs['ref_ratio'] = 2

    boxes_2 = lev_2.create_dataset('boxes', (3,), dtype=boxtype)

    boxes_2[0] = BOXES[2][0]
    boxes_2[1] = BOXES[2][1]
    boxes_2[2] = BOXES[2][2]

    lev_2['data:datatype=0'] = DATA[2]

    chombo.close()
