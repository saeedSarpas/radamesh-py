"""Testing main.py, fill_boxes method."""

from ..main import RadameshPy
import radamesh_py.tests.chombo as chombo


def test_if_it_fills_all_boxes_from_memory():
    rp = RadameshPy(chombo.FILENAME)
    rp.load ()
    rp.fill_boxes ()

    for il, level in enumerate(rp.levels):
        for ib, box in enumerate(level['boxes']):
            dims = [ y - x + 1 for x,y in zip (
                chombo.BOXES[il][ib][:2], chombo.BOXES[il][ib][2:])
            ]
            assert set ( box['field_0'].shape ) == set ( dims )
            assert box['field_0'][0,0] == il + 0.1 * ib + 0.01
            assert box['field_1'][0,0] == il + 0.1 * ib + 0.02
            assert box['field_0'][-1,-1] == il + 0.1 * ib + 0.01
            assert box['field_1'][-1,-1] == il + 0.1 * ib + 0.02
