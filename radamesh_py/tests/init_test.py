"""Testing main.py, RadameshPy initializations."""

from ..main import RadameshPy
import radamesh_py.tests.chombo as chombo
import astropy.units as u


def test_if_it_loads_attributes():
    attrs = [
        ('num_levels', 3),
        ('num_components', 2),
        ('component_0', 'field_0'),
        ('component_1', 'field_1'),
        ('iteration', 1),
        ('redshift', chombo.REDSHIFT),
        ('ComovingBoxSize', chombo.BOXSIZE)
    ]

    rp = RadameshPy(chombo.FILENAME)

    for attr in attrs:
        assert rp.attrs[attr[0]] == attr[1]

    assert ( rp.attrs['ProblemDomain'] == [4, 4] ).all()
    assert rp.comoving_box_size.value == chombo.BOXSIZE
    assert rp.comoving_box_size.unit == u.Mpc
    assert rp.z == chombo.REDSHIFT

    assert len ( rp.levels ) is chombo.NUM_LEVELS

    fields = [ 'boxes', 'boxes_coor', 'attrs' ]

    for l in range ( chombo.NUM_LEVELS ):
        assert all ( (f in rp.levels[l]) is True for f in fields )
