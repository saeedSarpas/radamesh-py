"""Testing main.py, load method."""

from ..main import RadameshPy
import radamesh_py.tests.chombo as chombo


def test_if_it_loads_different_levels_properly():
    rp = RadameshPy(chombo.FILENAME)
    rp.load()

    for l in range(chombo.NUM_LEVELS):
        attrs = rp.levels[l]['attrs']
        data = rp.levels[l]['data']
        boxes = rp.levels[l]['boxes']
        boxes_coor = rp.levels[l]['boxes_coor']

        assert attrs['dx'] == (chombo.INIT_DX / 2**l)
        assert attrs['ref_ratio'] == 2
        assert len ( boxes ) == len ( chombo.BOXES[l] )

        if l == 0:
            assert attrs['prob_domain'] == chombo.INIT_PROB_DOMAIN

        for i in range(len(chombo.BOXES[l])):
            assert set(boxes_coor[i]) == set(chombo.BOXES[l][i])

        assert set ( data ) == set ( chombo.DATA[l] )
